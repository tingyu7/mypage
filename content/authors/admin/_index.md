---
# Display name
title: Jacky Wang

# Username (this should match the folder name)
authors:
- admin

# Is this the primary user of the site?
superuser: true

# Role/position
role: Embedded Software / Firmware Engineer

# Organizations/Affiliations
organizations:
- name: Gogoro
  url: "https://www.gogoro.com/"

# Short bio (displayed in user profile at end of posts)
#bio: My research interests include distributed robotics, mobile computing and programmable matter.

interests:
- Embedded Systems
- Real-Time Operating System
- Multithreaded Programming
- Wireless Devices

education:
  courses:
  - course: MSc in Electrical and Computer Engineering
    institution: Carnegie Mellon University
    year: Expected Dec. 2022
  - course: BSc in Electrical Engineering
    institution: University of Washington
    year: 2017

# Social/Academic Networking
# For available icons, see: https://sourcethemes.com/academic/docs/page-builder/#icons
#   For an email link, use "fas" icon pack, "envelope" icon, and a link in the
#   form "mailto:your-email@example.com" or "#contact" for contact widget.
social:
- icon: linkedin
  icon_pack: fab
  link: https://www.linkedin.com/in/jacky-wang
- icon: gitlab
  icon_pack: fab
  link: https://gitlab.com/tingyu7
- icon: envelope
  icon_pack: fas
  link: '#contact'  # For a direct email link, use "mailto:test@example.org".  
# Link to a PDF of your resume/CV from the About widget.
# To enable, copy your resume/CV to `static/files/cv.pdf` and uncomment the lines below.
- icon: cv
  icon_pack: ai
  link: files/resume.pdf

# Enter email to display Gravatar (if Gravatar enabled in Config)
email: "tingyuwa@andrew.cmu.edu"

# Organizational groups that you belong to (for People widget)
#   Set this to `[]` or comment out if you are not using People widget.
# user_groups:
# - Researchers
# - Visitors
---

I enjoy working on projects in which I can utilize and expand my knowledge in embedded systems, be it for automobiles, medical equipment, energy systems, robotics or consumer electronics.

I like designing and writing software for microcontroller based embedded systems. My primary language is C and my favorite unit testing framework is Ceedling.
