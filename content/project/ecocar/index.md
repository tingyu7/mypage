---
title: EcoCAR
summary: Collegiate Advanced Vehicle Technology Competition 
tags:
- Automotive
- Hardware
date: "2016-05-21T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  #caption: Photo by rawpixel on Unsplash
  focal_point: Smart

links:

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
The EcoCAR competition has been the longest project I have ever worked on in college. It not only provided me with enormous amount of hands-on experience, but also got me interested in hybrid and electric vehicles in general.

I started out as a freshman on the electrical team working on both high and low voltage electrical systems integration before becoming a sub-team lead. 

![Motor Testing](motor_testing.jpg)
![ESS Integration](battery_integration_in_action.jpg)
![ESS Complete](battery_integration_complete.jpg)
![LV Integration](lv_integration.jpg)

Eventually I became part of the innovation team developing a proof of concept for a predictive generator control strategy for our vehicle.

I was also very grateful to have the opportunity to attend one of the final competition events in GM's Desert Proving Ground.

![Competition](comp.jpg)
![Jump](group_jump.jpg)