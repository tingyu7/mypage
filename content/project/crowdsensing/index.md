---
title: UW Capstone Project - CrowdSensing
summary: Air quality multi-sensor board design and development 
tags:
- Hardware
- Embedded
date: "2017-04-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  #caption: Photo by rawpixel on Unsplash
  focal_point: Smart

links:

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
The CrowdSensing Project involves design and developing a air quality multi-sensor board that is capable of detecting air quality and classifying pollutants. The project is divided into four components: the hardware, firmware, mobile application and backend server for data storage.

I was mainly playing the role of the project manager overseeing the schedule of the entire project. Moreover, I was also responsible for the hardware design, hardware / software integration and board bring-up.

![Demo](crowdsensing_showcase.jpg)