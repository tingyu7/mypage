---
title: Advanced Vehicle Control Lab
summary: Vehicle simulation and self-driving car development  
tags:
- Automotive
- Software
date: "2015-08-01T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  #caption: Photo by rawpixel on Unsplash
  focal_point: Smart

links:

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
Having developed interest in the automotive industry through the EcoCAR project, I decided to go for an automotive related internship for Summer 2015. During my time in the Advanced Vehicle Control Lab at the National Taipei University of Technology, I learned about modeling and simulation for hybrid electric vehicle using Matlab Simulink. In addition, I also worked on setting up the CAN communication for a vehicle model where I gained a deeper understanding of how CAN works in a vehicle.

Besides simulation work, I had the opportunity to program an autonomous mini car using Matlab which was able to drive itself through line scanning camera.