---
title: UW Embedded Systems Course Project
summary: Wireless and autonomous vehicle development 
tags:
- Embedded
- C
date: "2017-01-30T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  #caption: Photo by rawpixel on Unsplash
  focal_point: Smart

links:

url_code: "https://gitlab.com/tingyu7/ee474_final_project"
url_pdf: "https://gitlab.com/tingyu7/ee474_final_project/-/blob/master/lab_5_report.pdf"
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
This project involves building a wirelessly controlled vehicle that is also capable of driving itself using a BeagleBone Black microprocessor running embedded Linux, Raspberry Pi and Pi camera, DC motor controller, LCD display and Bluetooth module.
I was mainly working on the firmware for the LCD display, motor control and wireless communication.

![Wireless Car In Action](wireless_car_in_action.jpg)

Please refer to the project document and source code links under the title for this page.