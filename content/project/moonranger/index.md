---
title: MoonRanger
summary: A rover being developed by Carnegie Mellon University and its spinoff Astrobotic that will be sent to the moon's south pole to search for signs of water
tags:
- Embedded
- Software
- C
date: "2021-07-17T00:00:01Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by CMU MoonRanger Team
  focal_point: Smart

links:

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
Under Construction