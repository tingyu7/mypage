---
title: Daimler Trucks North America
summary: Truck performance simulation and data analysis 
tags:
- Automotive
- Software
- Python
date: "2016-07-11T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by Daimler Trucks North America
  focal_point: Smart

links:

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

My responsibilies include running software-in-the-loop simulations using Matlab, processing simulation data using Python and Excel and then present findings to aid research and development for current and future trucks.