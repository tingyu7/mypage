---
title: Project Amalgam (Paused)
summary: Simple electric vehicle superviosry controller and keyfob
tags:
- Embedded
- Software
- C
date: "2020-05-03T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  #caption: Photo by rawpixel on Unsplash
  focal_point: Smart

links:

url_project: "https://docs.google.com/document/d/13K3yeCzsxTxadv1jdjtT-GRe-Cr_P333t12WVlM3H0U/edit?usp=sharing"
url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---

Project Paused

This is a side project to showcase many of the firmware and embedded software concepts and techniques that I have acquired from classroom and work experience. The goal of this project is to create a simple electric vehicle controller and a keyfob. This project will involve bootloader design, device driver bring-up, application design, as well as usage of a real time OS. Additionally, unit testing will be adopted and a GUI tool will be developed to assist firmware upgrade, virtual dashboard display and integration testing. For more details on the project, please refer to the design document on the "Project" link below the title of this page.