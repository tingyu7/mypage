---
title: Gogoro
summary: Gogoro energy product embedded software and firmware development 
tags:
- Energy
- Embedded
- Software
- C
- Python
date: "2021-07-17T00:00:00Z"

# Optional external URL for project (replaces project detail page).
external_link: ""

image:
  caption: Photo by Gogoro
  focal_point: Smart

links:

url_code: ""
url_pdf: ""
url_slides: ""
url_video: ""

# Slides (optional).
#   Associate this project with Markdown slides.
#   Simply enter your slide deck's filename without extension.
#   E.g. `slides = "example-slides"` references `content/slides/example-slides.md`.
#   Otherwise, set `slides = ""`.
slides: ""
---
Gogoro is an energy platform company with a goal to provide energy solution for other businesses and consumers.

My work involves embedded software and firmware development of Gogoro energy products such as the battery packs, battery charging stations as well as diagnostic toolkit. However, I mainly focused on the battery management system. This meant that I often collaborate with cross-functional teams in coming up with and improving existing battery protection and gauging algorithms.

Some of the coolest projects that I have worked on include a portable and reusable hardware abstraction layer for all Gogoro energy products and the design and implementation of the safety firmware update mechanism which drastically reduced the update failure rate.

For team productivity, I have introduced and integrated unit testing to our development process and the CI system. This significantly increased our confidence with our software and reduced our time in debugging. Moreover, I have also introduced and integrated core dump to our log system to enable easier debugging and constant monitoring both in the test environment and in the market.

Some of the peripheral devices that I got to interact with include flash memory, crypto IC, real-time clock, battery gauging IC and NFC IC.